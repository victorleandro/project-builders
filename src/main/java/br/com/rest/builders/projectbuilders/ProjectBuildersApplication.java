package br.com.rest.builders.projectbuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectBuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectBuildersApplication.class, args);
	}

}
